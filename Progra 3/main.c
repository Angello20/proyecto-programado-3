#include <stdio.h>
#include <stdlib.h>
 
// Estructura para representa un nodo en una lista de adyacencia
struct NodoListAd
{
    int destino;
    struct NodoListAd* sigt;
};
 
//Estructura para representar una lista de adyacencia
struct AdLista
{
    struct NodoListAd *cabeza;
};
 
// Estructura para representar un grafo.
// un grafo es un arreglo de listas de adyacencias.
// Tamaño del arreglo será V (número de vértices en el grafo)
struct Grafo
{
    int V;
    struct AdLista* array;
};
 
// Una función para crear un nuevo nodo de lista de adyacencia
struct NodoListAd* nuevoNodoListAd(int destino)
{
    struct NodoListAd* nuevo_nodo =
     (struct NodoListAd*) malloc(sizeof(struct NodoListAd));
    nuevo_nodo->destino = destino;
    nuevo_nodo->sigt = NULL;
    return nuevo_nodo;
}
 
// Una función para crear un grafo de V vértices
struct Grafo* crearGrafo(int V)
{
    struct Grafo* grafo =
        (struct Grafo*) malloc(sizeof(struct Grafo));
    grafo->V = V;
 
    // Crear un arreglo de lista de adyacencia. El tamaño del arreglo será V
    grafo->array =
      (struct AdLista*) malloc(V * sizeof(struct AdLista));
 
    // Incializa cada lista de adyancencia vacía y con la cabeza en nulo
    int i;
    for (i = 0; i < V; ++i)
        grafo->array[i].cabeza = NULL;
 
    return grafo;
}
 
// Agregado una arista a un grafo no dirigido
void agregarArista(struct Grafo* grafo, int src, int destino)
{
    // Agrega una arista del src al destino. Un nuevo nodo se añade a la lista de adyacencia del src. El nodo se agrega al incio
    struct NodoListAd* nuevo_nodo = nuevoNodoListAd(destino);
    nuevo_nodo->sigt = grafo->array[src].cabeza;
    grafo->array[src].cabeza = nuevo_nodo;
 
    // Como el grafo no es dirigido, agrega una arista del destino al src también
    nuevo_nodo = nuevoNodoListAd(src);
    nuevo_nodo->sigt = grafo->array[destino].cabeza;
    grafo->array[destino].cabeza = nuevo_nodo;
}
 
// Función para imprimir un grafo
void ImprimirGrafo(struct Grafo* grafo)
{
    int v;
    for (v = 0; v < grafo->V; ++v)
    {
        struct NodoListAd* X = grafo->array[v].cabeza;
        printf("\n Lista de adyacencia del vertice %d\n cabeza ", v);
        while (X)
        {
            printf("-> %d", X->destino);
            X = X->sigt;
        }
        printf("\n");
    }
}
 
int main()
{
    int V = 5;
    struct Grafo* grafo = crearGrafo(V);
    agregarArista(grafo, 0, 1);
    agregarArista(grafo, 0, 4);
    agregarArista(grafo, 1, 2);
    agregarArista(grafo, 1, 3);
    agregarArista(grafo, 1, 4);
    agregarArista(grafo, 2, 3);
    agregarArista(grafo, 3, 4);
 
    ImprimirGrafo(grafo);
 
    return 0;
}